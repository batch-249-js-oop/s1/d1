//1. const exampleArray = [];
//2. exampleArray[0];
//3. exampleArray[exampleArray.length-1];
//4. Array.indexOf();
//5. Array.map();
//6. splice()
//7. Array.every();
//8. Array.some();
//9. f
//10. t

//Function COding

const students = ['Tess', 'John', 'Joe', 'Jane', 'Jessie', 'Ryan'];

//1
function addToEnd(array,student){
    if(typeof student !== 'string'){
        return 'error - can only add strings to an array';
    }
    array.push(student);
    return array;
}
//2
function addToStart(array,student){
    if(typeof student !== 'string'){
        return 'error - can only add strings to an array';
    }
    array.unshift(student);
    return array;
}
//3
function elementChecker(array,student){
    if(array.length===0){
        return 'error - passed array is empty';
    }
    //return (array.indexOf(student) === -1) ? false : true;
    // using loop:
    for(element of array){
        if(element.toLowerCase()===student.toLowerCase()){
            return true
        } else continue;
    }
    return false;
}


//4
function checkAllStringsEnding(array,str){
    if(array.length===0){
        return 'error - array must NOT be empty';
    }
    if(array.some(element=> typeof element !== 'string')){
        return 'error - all array elements must be strings';
    }
    if(typeof str !== 'string'){
        return 'error - 2nd argument must be of data type string';
    }
    if(str.length>1){
        return 'error - 2nd argument must be a single character';
    }
    if(array.every(element=>element[element.length-1]===str)){
        return true;
    } else {
        return false;
    }
}

//5
function stringLengthSorter(array){
    if(array.some(element=>typeof element !=='string')){
        return 'error - all array elements must be strings';
    }
    array.sort((a,b)=>a.length - b.length);
    return array;
}

//6
function startsWithCounter(array,str){
    if(array.length===0){
        return 'error - array must NOT be empty';
    }
    if(array.some(element=> typeof element !== 'string')){
        return 'error - all array elements must be strings';
    }
    if(typeof str !== 'string'){
        return 'error - 2nd argument must be of data type string';
    }
    if(str.length>1){
        return 'error - 2nd argument must be a single character';
    }
    //using loop:
    // let count = 0;
    // for(element of array){
    //     if(element[0].toLowerCase()===str.toLowerCase()){
    //         count++
    //     }
    // }
    // return count;

    //using array method:
    return array.filter((element)=>{
        return element.toLowerCase().startsWith(str.toLowerCase());
    }).length;

}

//7
function likeFinder(array,str){

    if(array.length===0){
        return 'error - array must NOT be empty';
    }
    if(array.some(element=> typeof element !== 'string')){
        return 'error - all array elements must be strings';
    }
    if(typeof str !== 'string'){
        return 'error - 2nd argument must be of data type string';
    }
    //using loop:
    // let newArr = [];
    // for(element of array){
    //     if(element.toLowerCase().startsWith(str.toLowerCase())){
    //         newArr.push(element)
    //     }
    // }
    // return newArr;

    //Using Array Methods:
    return array.filter((element) => {
        return element.toLowerCase().startsWith(str.toLowerCase())
    });
}

//8
function randomPicker(array){
    let randomIndex = Math.round(Math.random()*(array.length-1));
    return array[randomIndex];
}
